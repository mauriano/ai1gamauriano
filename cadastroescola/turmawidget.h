#ifndef TURMAWIDGET_H
#define TURMAWIDGET_H

#include <QWidget>
#include "escola.h"
#include <QList>


namespace Ui {
class TurmaWidget;
}

class TurmaWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TurmaWidget(QWidget *parent = nullptr);
    explicit TurmaWidget(QWidget *parent = nullptr, Escola *escola = nullptr);
    ~TurmaWidget();

private slots:
    void on_pushButton_clicked();

    void on_addAluno_clicked();

    void on_addProfessor_clicked();

private:
    Ui::TurmaWidget *ui;
    void salvarDados();
    Escola *escola;
    Professor professor;
    QList<Aluno> alunos;
};

#endif // TURMAWIDGET_H

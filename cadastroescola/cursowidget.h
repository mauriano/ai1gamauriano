#ifndef CURSOWIDGET_H
#define CURSOWIDGET_H

#include <QWidget>
#include "escola.h"
#include <QList>

namespace Ui {
class CursoWidget;
}

class CursoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CursoWidget(QWidget *parent = nullptr);
    explicit CursoWidget(QWidget *parent = nullptr, Escola *escola = nullptr);
    ~CursoWidget();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::CursoWidget *ui;
    void salvarDados();
    Escola *escola;
    QList<Disciplina> disciplinas;

};

#endif // CURSOWIDGET_H

#ifndef DISCIPLINAWIDGET_H
#define DISCIPLINAWIDGET_H

#include <QWidget>
#include "escola.h"
#include <QList>

namespace Ui {
class DisciplinaWidget;
}

class DisciplinaWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DisciplinaWidget(QWidget *parent = nullptr);
    explicit DisciplinaWidget(QWidget *parent = nullptr, Escola *escola = nullptr);
    ~DisciplinaWidget();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::DisciplinaWidget *ui;
    void salvarDados();
    Escola *escola;
    QList<Turma> turmas;

};

#endif // DISCIPLINAWIDGET_H

#include "alunowidget.h"
#include "ui_alunowidget.h"

AlunoWidget::AlunoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AlunoWidget)
{
    ui->setupUi(this);
}

AlunoWidget::AlunoWidget(QWidget *parent, Escola * escola) :
    QWidget(parent),
    ui(new Ui::AlunoWidget)
{
    ui->setupUi(this);
    this->escola = escola;
}


AlunoWidget::~AlunoWidget()
{
    delete ui;
}

void AlunoWidget::salvarDados()
{
    int anoInicio = ui->editAnoInicio->text().toInt();
    int semestreInicio = ui->editSemestreInicio->text().toInt();
    QString situacao = ui->editSituacao->text();
    QString matricula = ui->editMatricula->text();

    QString nome = ui->editNome->text();
    QDate data = QDate::fromString(ui->editNascimento->text(),"dd/MM/yyyy");
    QString email = ui->editEmail->text();
    QString end = ui->editEndereco->text();
    QString tel = ui->editTelefone->text();
    QString foto = ui->editFoto->text();
    QString cpf = ui->editCpf->text();
    QString rg = ui->editRg->text();
    Pessoa *pessoa = new Pessoa(nome,data,email,end,tel,foto,cpf,rg);

    Aluno *aluno = new Aluno(matricula, anoInicio, semestreInicio, situacao, *pessoa);

    escola->addAluno(*aluno);
}


void AlunoWidget::on_pushButton_clicked()
{
    salvarDados();
    this->hide();
}

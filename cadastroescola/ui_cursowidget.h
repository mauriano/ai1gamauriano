/********************************************************************************
** Form generated from reading UI file 'cursowidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CURSOWIDGET_H
#define UI_CURSOWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CursoWidget
{
public:
    QLineEdit *lineEdit_5;
    QLabel *label_14;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton;
    QLabel *label_10;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit;
    QLabel *label_12;
    QLineEdit *lineEdit_8;
    QLabel *label_8;
    QLabel *label;
    QLineEdit *lineEdit_6;
    QComboBox *comboDisciplinas;
    QLabel *label_15;
    QPushButton *pushButton_2;

    void setupUi(QWidget *CursoWidget)
    {
        if (CursoWidget->objectName().isEmpty())
            CursoWidget->setObjectName(QStringLiteral("CursoWidget"));
        CursoWidget->resize(505, 341);
        lineEdit_5 = new QLineEdit(CursoWidget);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(357, 100, 113, 20));
        label_14 = new QLabel(CursoWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(280, 100, 71, 23));
        lineEdit_2 = new QLineEdit(CursoWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(110, 100, 113, 20));
        pushButton = new QPushButton(CursoWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(230, 290, 81, 41));
        label_10 = new QLabel(CursoWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(57, 71, 47, 23));
        lineEdit_13 = new QLineEdit(CursoWidget);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setGeometry(QRect(80, 20, 351, 31));
        QFont font;
        font.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font.setPointSize(12);
        lineEdit_13->setFont(font);
        lineEdit_13->setFrame(false);
        lineEdit_13->setAlignment(Qt::AlignCenter);
        lineEdit = new QLineEdit(CursoWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(110, 70, 113, 20));
        label_12 = new QLabel(CursoWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(270, 71, 81, 23));
        lineEdit_8 = new QLineEdit(CursoWidget);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));
        lineEdit_8->setGeometry(QRect(357, 70, 113, 20));
        label_8 = new QLabel(CursoWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 100, 101, 23));
        label = new QLabel(CursoWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 190, 91, 41));
        lineEdit_6 = new QLineEdit(CursoWidget);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(100, 170, 341, 101));
        comboDisciplinas = new QComboBox(CursoWidget);
        comboDisciplinas->setObjectName(QStringLiteral("comboDisciplinas"));
        comboDisciplinas->setGeometry(QRect(230, 140, 111, 22));
        label_15 = new QLabel(CursoWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(160, 140, 71, 23));
        pushButton_2 = new QPushButton(CursoWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(360, 140, 61, 21));

        retranslateUi(CursoWidget);

        QMetaObject::connectSlotsByName(CursoWidget);
    } // setupUi

    void retranslateUi(QWidget *CursoWidget)
    {
        CursoWidget->setWindowTitle(QApplication::translate("CursoWidget", "Cadastro de Cursos", Q_NULLPTR));
        label_14->setText(QApplication::translate("CursoWidget", "Tipo curso", Q_NULLPTR));
        pushButton->setText(QApplication::translate("CursoWidget", "Confirma", Q_NULLPTR));
        label_10->setText(QApplication::translate("CursoWidget", "C\303\263digo", Q_NULLPTR));
        lineEdit_13->setText(QApplication::translate("CursoWidget", "Cadastro de cursos", Q_NULLPTR));
        label_12->setText(QApplication::translate("CursoWidget", "Carga Hor\303\241ria", Q_NULLPTR));
        label_8->setText(QApplication::translate("CursoWidget", "Qtd de per\303\255odos", Q_NULLPTR));
        label->setText(QApplication::translate("CursoWidget", "Descri\303\247\303\243o", Q_NULLPTR));
        label_15->setText(QApplication::translate("CursoWidget", "Disciplinas", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("CursoWidget", "Adicionar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CursoWidget: public Ui_CursoWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CURSOWIDGET_H

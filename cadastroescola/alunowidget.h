#ifndef ALUNOWIDGET_H
#define ALUNOWIDGET_H

#include <QWidget>
#include "escola.h"
#include <QList>

namespace Ui {
class AlunoWidget;
}

class AlunoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AlunoWidget(QWidget *parent = nullptr);
    explicit AlunoWidget(QWidget *parent = nullptr, Escola *escola = nullptr);
    ~AlunoWidget();

private slots:
    void on_pushButton_clicked();

private:
    Ui::AlunoWidget *ui;
    void salvarDados();
    Escola* escola;
};

#endif // ALUNOWIDGET_H

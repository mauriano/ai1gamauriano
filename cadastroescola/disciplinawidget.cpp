#include "disciplinawidget.h"
#include "ui_disciplinawidget.h"

DisciplinaWidget::DisciplinaWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DisciplinaWidget)
{
    ui->setupUi(this);
}

DisciplinaWidget::DisciplinaWidget(QWidget *parent, Escola * escola) :
    QWidget(parent),
    ui(new Ui::DisciplinaWidget)
{
    ui->setupUi(this);
    this->escola = escola;

    for (int i=0;i<this->escola->getTurmas().size();i++) {
        ui->comboTurmas->addItem(escola->getTurmas().takeAt(i).getDescricao());
    }
}


DisciplinaWidget::~DisciplinaWidget()
{
    delete ui;
}

void DisciplinaWidget::salvarDados()
{
    QString codigo = ui->lineEdit->text();
    QString descricao = ui->lineEdit_3->text();
    int periodo = ui->lineEdit_2->text().toInt();
    int numAulas = ui->lineEdit_8->text().toInt();
    QString emenda = ui->lineEdit_5->text();
    QString bibliografia = ui->lineEdit_4->text();

    Disciplina *disciplina = new Disciplina(codigo, descricao, periodo, numAulas, emenda, bibliografia);
    for (int i = 0; i<turmas.size();i++) {
        disciplina->addTurma(turmas.takeAt(i));
    }

    escola->addDisciplina(*disciplina);
}


void DisciplinaWidget::on_pushButton_clicked()
{
    salvarDados();
    this->hide();
}

void DisciplinaWidget::on_pushButton_2_clicked()
{
    turmas.push_back(this->escola->getTurmas().takeAt(ui->comboTurmas->currentIndex()));
}

#include "cursowidget.h"
#include "ui_cursowidget.h"

CursoWidget::CursoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CursoWidget)
{
    ui->setupUi(this);
}


CursoWidget::CursoWidget(QWidget *parent, Escola *escola) :
    QWidget(parent),
    ui(new Ui::CursoWidget)
{
    ui->setupUi(this);
    this->escola = escola;

    for (int i=0;i<this->escola->getDisciplinas().size();i++) {
        ui->comboDisciplinas->addItem(escola->getDisciplinas().takeAt(i).getDescricao());
    }
}

CursoWidget::~CursoWidget()
{
    delete ui;
}

void CursoWidget::salvarDados()
{
    int codigo = ui->lineEdit->text().toInt();
    QString descricao = ui->lineEdit_6->text();;
    int cargaHoraria = ui->lineEdit_8->text().toInt();
    int qtdPeriodos = ui->lineEdit_2->text().toInt();
    QString tpoCurso = ui->lineEdit_5->text();


    Curso *curso = new Curso(codigo, descricao, cargaHoraria, qtdPeriodos, tpoCurso);
    for (int i = 0; i<disciplinas.size();i++) {
        curso->addDisciplina(disciplinas.takeAt(i));
    }

    escola->addCurso(*curso);
}

void CursoWidget::on_pushButton_clicked()
{
    salvarDados();
    this->hide();
}

void CursoWidget::on_pushButton_2_clicked()
{
    disciplinas.push_back(this->escola->getDisciplinas().takeAt(ui->comboDisciplinas->currentIndex()));
}

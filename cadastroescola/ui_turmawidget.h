/********************************************************************************
** Form generated from reading UI file 'turmawidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TURMAWIDGET_H
#define UI_TURMAWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TurmaWidget
{
public:
    QPushButton *pushButton;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_8;
    QLabel *label_12;
    QLabel *label_10;
    QLineEdit *lineEdit_2;
    QLabel *label_8;
    QLabel *label;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit_3;
    QLabel *label_9;
    QLabel *label_11;
    QComboBox *comboAluno;
    QComboBox *comboProfessor;
    QPushButton *addProfessor;
    QPushButton *addAluno;

    void setupUi(QWidget *TurmaWidget)
    {
        if (TurmaWidget->objectName().isEmpty())
            TurmaWidget->setObjectName(QStringLiteral("TurmaWidget"));
        TurmaWidget->resize(496, 315);
        pushButton = new QPushButton(TurmaWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(210, 260, 81, 41));
        lineEdit = new QLineEdit(TurmaWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(80, 70, 113, 20));
        lineEdit_8 = new QLineEdit(TurmaWidget);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));
        lineEdit_8->setGeometry(QRect(337, 70, 113, 20));
        label_12 = new QLabel(TurmaWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(210, 71, 121, 23));
        label_10 = new QLabel(TurmaWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(27, 71, 47, 23));
        lineEdit_2 = new QLineEdit(TurmaWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(190, 110, 113, 20));
        label_8 = new QLabel(TurmaWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(120, 110, 61, 23));
        label = new QLabel(TurmaWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 190, 91, 41));
        lineEdit_13 = new QLineEdit(TurmaWidget);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setGeometry(QRect(90, 20, 351, 31));
        QFont font;
        font.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font.setPointSize(12);
        lineEdit_13->setFont(font);
        lineEdit_13->setFrame(false);
        lineEdit_13->setAlignment(Qt::AlignCenter);
        lineEdit_3 = new QLineEdit(TurmaWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(90, 170, 331, 81));
        label_9 = new QLabel(TurmaWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 140, 61, 23));
        label_11 = new QLabel(TurmaWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(270, 140, 61, 23));
        comboAluno = new QComboBox(TurmaWidget);
        comboAluno->setObjectName(QStringLiteral("comboAluno"));
        comboAluno->setGeometry(QRect(300, 140, 111, 22));
        comboProfessor = new QComboBox(TurmaWidget);
        comboProfessor->setObjectName(QStringLiteral("comboProfessor"));
        comboProfessor->setGeometry(QRect(60, 140, 111, 22));
        addProfessor = new QPushButton(TurmaWidget);
        addProfessor->setObjectName(QStringLiteral("addProfessor"));
        addProfessor->setGeometry(QRect(180, 140, 61, 21));
        addAluno = new QPushButton(TurmaWidget);
        addAluno->setObjectName(QStringLiteral("addAluno"));
        addAluno->setGeometry(QRect(420, 140, 61, 21));

        retranslateUi(TurmaWidget);

        QMetaObject::connectSlotsByName(TurmaWidget);
    } // setupUi

    void retranslateUi(QWidget *TurmaWidget)
    {
        TurmaWidget->setWindowTitle(QApplication::translate("TurmaWidget", "Cadastro de turmas", Q_NULLPTR));
        pushButton->setText(QApplication::translate("TurmaWidget", "Confirma", Q_NULLPTR));
        label_12->setText(QApplication::translate("TurmaWidget", "Num m\303\241ximo alunos", Q_NULLPTR));
        label_10->setText(QApplication::translate("TurmaWidget", "Ano", Q_NULLPTR));
        label_8->setText(QApplication::translate("TurmaWidget", "Semestre", Q_NULLPTR));
        label->setText(QApplication::translate("TurmaWidget", "Descri\303\247\303\243o", Q_NULLPTR));
        lineEdit_13->setText(QApplication::translate("TurmaWidget", "Cadastro de turmas", Q_NULLPTR));
        label_9->setText(QApplication::translate("TurmaWidget", "Professor", Q_NULLPTR));
        label_11->setText(QApplication::translate("TurmaWidget", "Aluno", Q_NULLPTR));
        addProfessor->setText(QApplication::translate("TurmaWidget", "Definir", Q_NULLPTR));
        addAluno->setText(QApplication::translate("TurmaWidget", "Adicionar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class TurmaWidget: public Ui_TurmaWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TURMAWIDGET_H

/********************************************************************************
** Form generated from reading UI file 'professorwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROFESSORWIDGET_H
#define UI_PROFESSORWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProfessorWidget
{
public:
    QPushButton *pushButton;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLineEdit *editNome;
    QLineEdit *editNascimento;
    QLineEdit *editEmail;
    QLineEdit *editEndereco;
    QLineEdit *editTelefone;
    QLabel *label_11;
    QLabel *label_12;
    QLineEdit *editCpf;
    QLabel *label_13;
    QLineEdit *editRg;
    QLabel *label_14;
    QLineEdit *editFotografia;
    QLineEdit *editMatricula;
    QLabel *label_15;
    QLineEdit *editTitulo;
    QLineEdit *editContrato;
    QLabel *label_16;
    QLineEdit *editSalario;
    QLabel *label_17;
    QLineEdit *lineEdit_13;

    void setupUi(QWidget *ProfessorWidget)
    {
        if (ProfessorWidget->objectName().isEmpty())
            ProfessorWidget->setObjectName(QStringLiteral("ProfessorWidget"));
        ProfessorWidget->resize(466, 370);
        QFont font;
        font.setFamily(QStringLiteral("MS Shell Dlg 2"));
        ProfessorWidget->setFont(font);
        pushButton = new QPushButton(ProfessorWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(200, 310, 81, 41));
        label_6 = new QLabel(ProfessorWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(50, 160, 61, 23));
        label_7 = new QLabel(ProfessorWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(57, 130, 47, 23));
        label_8 = new QLabel(ProfessorWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 100, 101, 23));
        label_9 = new QLabel(ProfessorWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(250, 160, 61, 23));
        label_10 = new QLabel(ProfessorWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(57, 71, 47, 23));
        editNome = new QLineEdit(ProfessorWidget);
        editNome->setObjectName(QStringLiteral("editNome"));
        editNome->setGeometry(QRect(110, 70, 113, 20));
        editNascimento = new QLineEdit(ProfessorWidget);
        editNascimento->setObjectName(QStringLiteral("editNascimento"));
        editNascimento->setGeometry(QRect(110, 100, 113, 20));
        editEmail = new QLineEdit(ProfessorWidget);
        editEmail->setObjectName(QStringLiteral("editEmail"));
        editEmail->setGeometry(QRect(110, 130, 113, 20));
        editEndereco = new QLineEdit(ProfessorWidget);
        editEndereco->setObjectName(QStringLiteral("editEndereco"));
        editEndereco->setGeometry(QRect(320, 160, 113, 20));
        editTelefone = new QLineEdit(ProfessorWidget);
        editTelefone->setObjectName(QStringLiteral("editTelefone"));
        editTelefone->setGeometry(QRect(110, 160, 113, 20));
        label_11 = new QLabel(ProfessorWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(267, 220, 47, 23));
        label_12 = new QLabel(ProfessorWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(253, 71, 61, 23));
        editCpf = new QLineEdit(ProfessorWidget);
        editCpf->setObjectName(QStringLiteral("editCpf"));
        editCpf->setGeometry(QRect(320, 100, 113, 20));
        label_13 = new QLabel(ProfessorWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(50, 220, 61, 23));
        editRg = new QLineEdit(ProfessorWidget);
        editRg->setObjectName(QStringLiteral("editRg"));
        editRg->setGeometry(QRect(320, 130, 113, 20));
        label_14 = new QLabel(ProfessorWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(267, 100, 47, 23));
        editFotografia = new QLineEdit(ProfessorWidget);
        editFotografia->setObjectName(QStringLiteral("editFotografia"));
        editFotografia->setGeometry(QRect(320, 70, 113, 20));
        editMatricula = new QLineEdit(ProfessorWidget);
        editMatricula->setObjectName(QStringLiteral("editMatricula"));
        editMatricula->setGeometry(QRect(113, 220, 113, 20));
        label_15 = new QLabel(ProfessorWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(267, 130, 47, 23));
        editTitulo = new QLineEdit(ProfessorWidget);
        editTitulo->setObjectName(QStringLiteral("editTitulo"));
        editTitulo->setGeometry(QRect(320, 223, 113, 20));
        editContrato = new QLineEdit(ProfessorWidget);
        editContrato->setObjectName(QStringLiteral("editContrato"));
        editContrato->setGeometry(QRect(113, 250, 113, 20));
        label_16 = new QLabel(ProfessorWidget);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(267, 250, 47, 23));
        editSalario = new QLineEdit(ProfessorWidget);
        editSalario->setObjectName(QStringLiteral("editSalario"));
        editSalario->setGeometry(QRect(320, 253, 113, 20));
        label_17 = new QLabel(ProfessorWidget);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(50, 250, 61, 23));
        lineEdit_13 = new QLineEdit(ProfessorWidget);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setGeometry(QRect(80, 20, 351, 31));
        QFont font1;
        font1.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font1.setPointSize(12);
        lineEdit_13->setFont(font1);
        lineEdit_13->setFrame(false);
        lineEdit_13->setAlignment(Qt::AlignCenter);

        retranslateUi(ProfessorWidget);

        QMetaObject::connectSlotsByName(ProfessorWidget);
    } // setupUi

    void retranslateUi(QWidget *ProfessorWidget)
    {
        ProfessorWidget->setWindowTitle(QApplication::translate("ProfessorWidget", "Cadastro de professores", Q_NULLPTR));
        pushButton->setText(QApplication::translate("ProfessorWidget", "Confirma", Q_NULLPTR));
        label_6->setText(QApplication::translate("ProfessorWidget", "Telefone", Q_NULLPTR));
        label_7->setText(QApplication::translate("ProfessorWidget", "Email", Q_NULLPTR));
        label_8->setText(QApplication::translate("ProfessorWidget", "Data Nascimento", Q_NULLPTR));
        label_9->setText(QApplication::translate("ProfessorWidget", "Endere\303\247o", Q_NULLPTR));
        label_10->setText(QApplication::translate("ProfessorWidget", "Nome", Q_NULLPTR));
        label_11->setText(QApplication::translate("ProfessorWidget", "T\303\255tulo", Q_NULLPTR));
        label_12->setText(QApplication::translate("ProfessorWidget", "Fotografia", Q_NULLPTR));
        label_13->setText(QApplication::translate("ProfessorWidget", "Matricula", Q_NULLPTR));
        label_14->setText(QApplication::translate("ProfessorWidget", "CPF", Q_NULLPTR));
        label_15->setText(QApplication::translate("ProfessorWidget", "RG", Q_NULLPTR));
        label_16->setText(QApplication::translate("ProfessorWidget", "Sal\303\241rio", Q_NULLPTR));
        label_17->setText(QApplication::translate("ProfessorWidget", "Contrato", Q_NULLPTR));
        lineEdit_13->setText(QApplication::translate("ProfessorWidget", "Cadastro de professores", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ProfessorWidget: public Ui_ProfessorWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROFESSORWIDGET_H

#include "turmawidget.h"
#include "ui_turmawidget.h"

TurmaWidget::TurmaWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TurmaWidget)
{
    ui->setupUi(this);
}

TurmaWidget::TurmaWidget(QWidget *parent, Escola * escola) :
    QWidget(parent),
    ui(new Ui::TurmaWidget)
{
    ui->setupUi(this);
    this->escola = escola;

    for (int i=0;i<this->escola->getProfessores().size();i++) {
        ui->comboProfessor->addItem(escola->getProfessores().takeAt(i).getNome());
    }

    for (int i=0;i<this->escola->getAlunos().size();i++) {
        ui->comboAluno->addItem(escola->getAlunos().takeAt(i).getNome());
    }
}


TurmaWidget::~TurmaWidget()
{
    delete ui;
}

void TurmaWidget::salvarDados()
{
    int ano = ui->lineEdit->text().toInt();
    int semestre = ui->lineEdit_2->text().toInt();
    QString descricao = ui->lineEdit_3->text();
    int numMaxAlunos = ui->lineEdit_8->text().toInt();


    Turma *turma = new Turma(ano, semestre, descricao, numMaxAlunos);

    escola->addTurma(*turma);
}

void TurmaWidget::on_pushButton_clicked()
{
    salvarDados();
    this-> hide();
}

void TurmaWidget::on_addAluno_clicked()
{
    alunos.push_back(this->escola->getAlunos().takeAt(ui->comboAluno->currentIndex()));
}

void TurmaWidget::on_addProfessor_clicked()
{
    professor = this->escola->getProfessores().takeAt(ui->comboProfessor->currentIndex());
}

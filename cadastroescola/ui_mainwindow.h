/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAluno;
    QAction *actionProfessor;
    QAction *actionCurso;
    QAction *actionTurma;
    QAction *actionDisciplina;
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label;
    QTextEdit *listProfessor;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QLabel *label_2;
    QTextEdit *listAluno;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QLabel *label_3;
    QTextEdit *listCurso;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_4;
    QLabel *label_4;
    QTextEdit *listDisciplina;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_5;
    QLabel *label_5;
    QTextEdit *listTurma;
    QStatusBar *statusbar;
    QMenuBar *menuBar;
    QMenu *menuAdicionar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(822, 342);
        actionAluno = new QAction(MainWindow);
        actionAluno->setObjectName(QStringLiteral("actionAluno"));
        actionAluno->setCheckable(false);
        actionAluno->setChecked(false);
        actionProfessor = new QAction(MainWindow);
        actionProfessor->setObjectName(QStringLiteral("actionProfessor"));
        actionCurso = new QAction(MainWindow);
        actionCurso->setObjectName(QStringLiteral("actionCurso"));
        actionTurma = new QAction(MainWindow);
        actionTurma->setObjectName(QStringLiteral("actionTurma"));
        actionDisciplina = new QAction(MainWindow);
        actionDisciplina->setObjectName(QStringLiteral("actionDisciplina"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 20, 154, 261));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 1, 1, 1);

        listProfessor = new QTextEdit(gridLayoutWidget);
        listProfessor->setObjectName(QStringLiteral("listProfessor"));
        listProfessor->setEnabled(true);
        listProfessor->setReadOnly(true);

        gridLayout->addWidget(listProfessor, 1, 1, 1, 1);

        gridLayoutWidget_2 = new QWidget(centralwidget);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(170, 20, 151, 261));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(gridLayoutWidget_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);

        listAluno = new QTextEdit(gridLayoutWidget_2);
        listAluno->setObjectName(QStringLiteral("listAluno"));
        listAluno->setReadOnly(true);

        gridLayout_2->addWidget(listAluno, 1, 0, 1, 1);

        gridLayoutWidget_3 = new QWidget(centralwidget);
        gridLayoutWidget_3->setObjectName(QStringLiteral("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(330, 20, 151, 261));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(gridLayoutWidget_3);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_3->addWidget(label_3, 0, 0, 1, 1);

        listCurso = new QTextEdit(gridLayoutWidget_3);
        listCurso->setObjectName(QStringLiteral("listCurso"));
        listCurso->setReadOnly(true);

        gridLayout_3->addWidget(listCurso, 1, 0, 1, 1);

        gridLayoutWidget_4 = new QWidget(centralwidget);
        gridLayoutWidget_4->setObjectName(QStringLiteral("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(490, 20, 151, 261));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(gridLayoutWidget_4);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_4->addWidget(label_4, 0, 1, 1, 1);

        listDisciplina = new QTextEdit(gridLayoutWidget_4);
        listDisciplina->setObjectName(QStringLiteral("listDisciplina"));
        listDisciplina->setReadOnly(true);

        gridLayout_4->addWidget(listDisciplina, 1, 1, 1, 1);

        gridLayoutWidget_5 = new QWidget(centralwidget);
        gridLayoutWidget_5->setObjectName(QStringLiteral("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(650, 20, 151, 261));
        gridLayout_5 = new QGridLayout(gridLayoutWidget_5);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(gridLayoutWidget_5);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_5->addWidget(label_5, 0, 0, 1, 1);

        listTurma = new QTextEdit(gridLayoutWidget_5);
        listTurma->setObjectName(QStringLiteral("listTurma"));
        listTurma->setReadOnly(true);

        gridLayout_5->addWidget(listTurma, 1, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 822, 21));
        menuAdicionar = new QMenu(menuBar);
        menuAdicionar->setObjectName(QStringLiteral("menuAdicionar"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuAdicionar->menuAction());
        menuAdicionar->addAction(actionAluno);
        menuAdicionar->addAction(actionProfessor);
        menuAdicionar->addAction(actionCurso);
        menuAdicionar->addAction(actionTurma);
        menuAdicionar->addAction(actionDisciplina);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Escola", Q_NULLPTR));
        actionAluno->setText(QApplication::translate("MainWindow", "Aluno", Q_NULLPTR));
        actionProfessor->setText(QApplication::translate("MainWindow", "Professor", Q_NULLPTR));
        actionCurso->setText(QApplication::translate("MainWindow", "Curso", Q_NULLPTR));
        actionTurma->setText(QApplication::translate("MainWindow", "Turma", Q_NULLPTR));
        actionDisciplina->setText(QApplication::translate("MainWindow", "Disciplina", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Professores:", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Alunos:", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Cursos:", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Disciplinas", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Turmas", Q_NULLPTR));
        menuAdicionar->setTitle(QApplication::translate("MainWindow", "Adicionar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

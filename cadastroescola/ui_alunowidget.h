/********************************************************************************
** Form generated from reading UI file 'alunowidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ALUNOWIDGET_H
#define UI_ALUNOWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AlunoWidget
{
public:
    QLineEdit *editNome;
    QPushButton *pushButton;
    QLineEdit *editCpf;
    QLabel *label_7;
    QLineEdit *editFoto;
    QLabel *label_14;
    QLabel *label_9;
    QLineEdit *editNascimento;
    QLabel *label_6;
    QLabel *label_11;
    QLabel *label_16;
    QLineEdit *editAnoInicio;
    QLineEdit *editRg;
    QLabel *label_12;
    QLineEdit *editEndereco;
    QLineEdit *editMatricula;
    QLabel *label_17;
    QLineEdit *editEmail;
    QLineEdit *lineEdit_13;
    QLabel *label_10;
    QLabel *label_13;
    QLineEdit *editTelefone;
    QLabel *label_15;
    QLineEdit *editSemestreInicio;
    QLineEdit *editSituacao;
    QLabel *label_8;

    void setupUi(QWidget *AlunoWidget)
    {
        if (AlunoWidget->objectName().isEmpty())
            AlunoWidget->setObjectName(QStringLiteral("AlunoWidget"));
        AlunoWidget->resize(480, 339);
        editNome = new QLineEdit(AlunoWidget);
        editNome->setObjectName(QStringLiteral("editNome"));
        editNome->setGeometry(QRect(120, 50, 113, 20));
        pushButton = new QPushButton(AlunoWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(210, 270, 81, 41));
        editCpf = new QLineEdit(AlunoWidget);
        editCpf->setObjectName(QStringLiteral("editCpf"));
        editCpf->setGeometry(QRect(330, 80, 113, 20));
        label_7 = new QLabel(AlunoWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(67, 110, 47, 23));
        editFoto = new QLineEdit(AlunoWidget);
        editFoto->setObjectName(QStringLiteral("editFoto"));
        editFoto->setGeometry(QRect(330, 50, 113, 20));
        label_14 = new QLabel(AlunoWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(277, 80, 47, 23));
        label_9 = new QLabel(AlunoWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(260, 140, 61, 23));
        editNascimento = new QLineEdit(AlunoWidget);
        editNascimento->setObjectName(QStringLiteral("editNascimento"));
        editNascimento->setGeometry(QRect(120, 80, 113, 20));
        label_6 = new QLabel(AlunoWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(60, 140, 61, 23));
        label_11 = new QLabel(AlunoWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(263, 200, 61, 23));
        label_16 = new QLabel(AlunoWidget);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(273, 230, 51, 23));
        editAnoInicio = new QLineEdit(AlunoWidget);
        editAnoInicio->setObjectName(QStringLiteral("editAnoInicio"));
        editAnoInicio->setGeometry(QRect(330, 203, 113, 20));
        editRg = new QLineEdit(AlunoWidget);
        editRg->setObjectName(QStringLiteral("editRg"));
        editRg->setGeometry(QRect(330, 110, 113, 20));
        label_12 = new QLabel(AlunoWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(263, 51, 61, 23));
        editEndereco = new QLineEdit(AlunoWidget);
        editEndereco->setObjectName(QStringLiteral("editEndereco"));
        editEndereco->setGeometry(QRect(330, 140, 113, 20));
        editMatricula = new QLineEdit(AlunoWidget);
        editMatricula->setObjectName(QStringLiteral("editMatricula"));
        editMatricula->setGeometry(QRect(123, 200, 113, 20));
        label_17 = new QLabel(AlunoWidget);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(10, 230, 121, 23));
        editEmail = new QLineEdit(AlunoWidget);
        editEmail->setObjectName(QStringLiteral("editEmail"));
        editEmail->setGeometry(QRect(120, 110, 113, 20));
        lineEdit_13 = new QLineEdit(AlunoWidget);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setGeometry(QRect(90, 10, 351, 31));
        QFont font;
        font.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font.setPointSize(12);
        lineEdit_13->setFont(font);
        lineEdit_13->setFrame(false);
        lineEdit_13->setAlignment(Qt::AlignCenter);
        label_10 = new QLabel(AlunoWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(67, 51, 47, 23));
        label_13 = new QLabel(AlunoWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(60, 200, 61, 23));
        editTelefone = new QLineEdit(AlunoWidget);
        editTelefone->setObjectName(QStringLiteral("editTelefone"));
        editTelefone->setGeometry(QRect(120, 140, 113, 20));
        label_15 = new QLabel(AlunoWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(277, 110, 47, 23));
        editSemestreInicio = new QLineEdit(AlunoWidget);
        editSemestreInicio->setObjectName(QStringLiteral("editSemestreInicio"));
        editSemestreInicio->setGeometry(QRect(123, 230, 113, 20));
        editSituacao = new QLineEdit(AlunoWidget);
        editSituacao->setObjectName(QStringLiteral("editSituacao"));
        editSituacao->setGeometry(QRect(330, 233, 113, 20));
        label_8 = new QLabel(AlunoWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(20, 80, 101, 23));

        retranslateUi(AlunoWidget);

        QMetaObject::connectSlotsByName(AlunoWidget);
    } // setupUi

    void retranslateUi(QWidget *AlunoWidget)
    {
        AlunoWidget->setWindowTitle(QApplication::translate("AlunoWidget", "Cadastro de Alunos", Q_NULLPTR));
        pushButton->setText(QApplication::translate("AlunoWidget", "Confirma", Q_NULLPTR));
        label_7->setText(QApplication::translate("AlunoWidget", "Email", Q_NULLPTR));
        label_14->setText(QApplication::translate("AlunoWidget", "CPF", Q_NULLPTR));
        label_9->setText(QApplication::translate("AlunoWidget", "Endere\303\247o", Q_NULLPTR));
        label_6->setText(QApplication::translate("AlunoWidget", "Telefone", Q_NULLPTR));
        label_11->setText(QApplication::translate("AlunoWidget", "Ano Inicio", Q_NULLPTR));
        label_16->setText(QApplication::translate("AlunoWidget", "Situa\303\247\303\243o", Q_NULLPTR));
        label_12->setText(QApplication::translate("AlunoWidget", "Fotografia", Q_NULLPTR));
        label_17->setText(QApplication::translate("AlunoWidget", "Semestre de inicio", Q_NULLPTR));
        lineEdit_13->setText(QApplication::translate("AlunoWidget", "Cadastro de alunos", Q_NULLPTR));
        label_10->setText(QApplication::translate("AlunoWidget", "Nome", Q_NULLPTR));
        label_13->setText(QApplication::translate("AlunoWidget", "Matricula", Q_NULLPTR));
        label_15->setText(QApplication::translate("AlunoWidget", "RG", Q_NULLPTR));
        label_8->setText(QApplication::translate("AlunoWidget", "Data Nascimento", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AlunoWidget: public Ui_AlunoWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ALUNOWIDGET_H

/********************************************************************************
** Form generated from reading UI file 'disciplinawidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DISCIPLINAWIDGET_H
#define UI_DISCIPLINAWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DisciplinaWidget
{
public:
    QLabel *label_14;
    QLineEdit *lineEdit;
    QLabel *label_12;
    QLabel *label_10;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_13;
    QPushButton *pushButton;
    QLineEdit *lineEdit_8;
    QLabel *label;
    QLineEdit *lineEdit_5;
    QLabel *label_8;
    QLabel *label_2;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_4;
    QComboBox *comboTurmas;
    QPushButton *pushButton_2;
    QLabel *label_15;

    void setupUi(QWidget *DisciplinaWidget)
    {
        if (DisciplinaWidget->objectName().isEmpty())
            DisciplinaWidget->setObjectName(QStringLiteral("DisciplinaWidget"));
        DisciplinaWidget->resize(458, 337);
        label_14 = new QLabel(DisciplinaWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(240, 90, 71, 23));
        lineEdit = new QLineEdit(DisciplinaWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(70, 60, 113, 20));
        label_12 = new QLabel(DisciplinaWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(210, 61, 101, 23));
        label_10 = new QLabel(DisciplinaWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(17, 61, 47, 23));
        lineEdit_2 = new QLineEdit(DisciplinaWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(70, 90, 113, 20));
        lineEdit_13 = new QLineEdit(DisciplinaWidget);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setGeometry(QRect(67, 10, 351, 31));
        QFont font;
        font.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font.setPointSize(12);
        lineEdit_13->setFont(font);
        lineEdit_13->setFrame(false);
        lineEdit_13->setAlignment(Qt::AlignCenter);
        pushButton = new QPushButton(DisciplinaWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(320, 290, 81, 41));
        lineEdit_8 = new QLineEdit(DisciplinaWidget);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));
        lineEdit_8->setGeometry(QRect(317, 60, 113, 20));
        label = new QLabel(DisciplinaWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 130, 91, 41));
        lineEdit_5 = new QLineEdit(DisciplinaWidget);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(317, 90, 113, 20));
        label_8 = new QLabel(DisciplinaWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 90, 61, 23));
        label_2 = new QLabel(DisciplinaWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 210, 91, 41));
        lineEdit_3 = new QLineEdit(DisciplinaWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(110, 130, 291, 61));
        lineEdit_4 = new QLineEdit(DisciplinaWidget);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(110, 200, 291, 71));
        comboTurmas = new QComboBox(DisciplinaWidget);
        comboTurmas->setObjectName(QStringLiteral("comboTurmas"));
        comboTurmas->setGeometry(QRect(90, 300, 111, 22));
        pushButton_2 = new QPushButton(DisciplinaWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(220, 300, 61, 21));
        label_15 = new QLabel(DisciplinaWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(20, 300, 71, 23));

        retranslateUi(DisciplinaWidget);

        QMetaObject::connectSlotsByName(DisciplinaWidget);
    } // setupUi

    void retranslateUi(QWidget *DisciplinaWidget)
    {
        DisciplinaWidget->setWindowTitle(QApplication::translate("DisciplinaWidget", "Cadastro de disciplinas", Q_NULLPTR));
        label_14->setText(QApplication::translate("DisciplinaWidget", "Ementa", Q_NULLPTR));
        label_12->setText(QApplication::translate("DisciplinaWidget", "Numero de aulas", Q_NULLPTR));
        label_10->setText(QApplication::translate("DisciplinaWidget", "C\303\263digo", Q_NULLPTR));
        lineEdit_13->setText(QApplication::translate("DisciplinaWidget", "Cadastro de disciplinas", Q_NULLPTR));
        pushButton->setText(QApplication::translate("DisciplinaWidget", "Confirma", Q_NULLPTR));
        label->setText(QApplication::translate("DisciplinaWidget", "Descri\303\247\303\243o", Q_NULLPTR));
        label_8->setText(QApplication::translate("DisciplinaWidget", "Per\303\255odo", Q_NULLPTR));
        label_2->setText(QApplication::translate("DisciplinaWidget", "Bibliografia", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("DisciplinaWidget", "Adicionar", Q_NULLPTR));
        label_15->setText(QApplication::translate("DisciplinaWidget", "Turmas", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DisciplinaWidget: public Ui_DisciplinaWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DISCIPLINAWIDGET_H

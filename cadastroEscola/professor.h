#ifndef PROFESSOR_H
#define PROFESSOR_H

#include<pessoa.h>

class Professor: public Pessoa
{
public:
    Professor();
    Professor(QString mat,
              QString t,
              QString ti,
              float s,
              Pessoa pessoa);

    QString getMatricula();
    QString getTitulo();
    QString getTipoContrato();
    float getSalario();

    void setMatricula(QString mat);
    void setTitulo(QString t);
    void setTipoContrato(QString ti);
    void setSalario(float s);
    void setPessoa(Pessoa pessoa);

private:
    QString matricula;
    QString titulo;
    QString tipoContrato;
    float salario;
};

#endif // PROFESSOR_H

#include "pessoa.h"

Pessoa::Pessoa()
{
    setNome("n");
    setDataNascimento(QDate::currentDate());
    setEndereco("");
    setEmail("");
    setTelefone("");
    setFotografia("");
    setCpf("");
    setRg("");
}

Pessoa::Pessoa(QString n,
               QDate da,
               QString e,
               QString en,
               QString t,
               QString f,
               QString c,
               QString r){
    setNome(n);
    setDataNascimento(da);
    setEmail(e);
    setEndereco(en);
    setTelefone(t);
    setFotografia(f);
    setCpf(c);
    setRg(r);
}

QString Pessoa::getNome(){
    return nome;
}
QDate Pessoa::getDataNascimento(){
    return dataNascimento;
}
QString Pessoa::getEmail(){
    return email;
}
QString Pessoa::getEndereco(){
    return endereco;
}
QString Pessoa::getTelefone(){
    return telefone;
}
QString Pessoa::getFotografia(){
    return fotografia;
}
QString Pessoa::getCpf(){
    return cpf;
}
QString Pessoa::getRg(){
    return rg;
}

void Pessoa::setNome(QString n){
   nome = n;
}
void Pessoa::setDataNascimento(QDate da){
    dataNascimento = da;
}
void Pessoa::setEmail(QString e){
    email = e;
}
void Pessoa::setEndereco(QString en){
    endereco = en;
}
void Pessoa::setTelefone(QString t){
    telefone = t;
}
void Pessoa::setFotografia(QString f){
    fotografia = f;
}
void Pessoa::setCpf(QString c){
    cpf = c;
}
void Pessoa::setRg(QString r){
    rg = r;
}



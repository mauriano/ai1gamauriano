#include "aluno.h"

Aluno::Aluno()
{
    this->setMatricula("");
    this->setSituacao("");
    this->setAnoInicio(0);
    this->setSemestreInicio(0);
}

Aluno::Aluno(QString mat,
      int a,
      int s,
      QString sit,
      Pessoa pessoa)
{
    this->setMatricula(mat);
    this->setSituacao(sit);
    this->setAnoInicio(a);
    this->setSemestreInicio(s);
    this->setPessoa(pessoa);
}

void Aluno::setPessoa(Pessoa pessoa){
    this->setNome(pessoa.getNome());
    this->setDataNascimento(pessoa.getDataNascimento());
    this->setEmail(pessoa.getEmail());
    this->setEndereco(pessoa.getEndereco());
    this->setTelefone(pessoa.getTelefone());
    this->setFotografia(pessoa.getFotografia());
    this->setRg(pessoa.getRg());
}

QString
Aluno::getMatricula()
{
    return this->matricula;
}

int
Aluno::getAnoInicio()
{
    return this->anoInicio;
}

int
Aluno::getSemestreInicio()
{
    return this->semestreInicio;
}

QString
Aluno::getSituacao()
{
    return this->situacao;
}

void
Aluno::setMatricula(QString mat)
{
    this->matricula = mat;
}

void
Aluno::setAnoInicio(int a)
{
    this->anoInicio = a;
}

void
Aluno::setSemestreInicio(int s)
{
    this-> semestreInicio = s;
}

void
Aluno::setSituacao(QString sit)
{
    this->situacao = sit;
}

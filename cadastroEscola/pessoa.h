#ifndef PESSOA_H
#define PESSOA_H

#include <QDate>
#include <QString>



class Pessoa
{
public:
    Pessoa();
    Pessoa(QString n,
           QDate da,
           QString e,
           QString en,
           QString t,
           QString f,
           QString c,
           QString r);

    QString getNome();
    QDate getDataNascimento();
    QString getEmail();
    QString getEndereco();
    QString getTelefone();
    QString getFotografia();
    QString getCpf();
    QString getRg();

    void setNome(QString n);
    void setDataNascimento(QDate da);
    void setEmail(QString e);
    void setEndereco(QString en);
    void setTelefone(QString t);
    void setFotografia(QString f);
    void setCpf(QString c);
    void setRg(QString r);

 private:
    QString nome;
    QDate dataNascimento;
    QString email;
    QString endereco;
    QString telefone;
    QString fotografia;
    QString cpf;
    QString rg;


};

#endif // PESSOA_H

#include "disciplina.h"

Disciplina::Disciplina()
{
    this->setCodigo("");
    this->setDescricao("");
    this->setPeriodo(0);
    this->setNumAulas(0);
    this->setEmenda("");
    this->setBibliografia("");
}

Disciplina::Disciplina(QString c,
           QString d,
           int p,
           int n,
           QString e,
           QString b)
{
    this->setCodigo(c);
    this->setDescricao(d);
    this->setPeriodo(p);
    this->setNumAulas(n);
    this->setEmenda(e);
    this->setBibliografia(b);
}

QString
Disciplina::getCodigo()
{
    return this->codigo;
}

QString
Disciplina::getDescricao()
{
    return this->descricao;
}

int
Disciplina::getPeriodo()
{
    return this->periodo;
}

int
Disciplina::getNumAulas()
{
    return this->numAulas;
}

QString
Disciplina::getEmenda()
{
    return this->emenda;
}

QString
Disciplina::getBibliografia()
{
    return this->bibliografia;
}

void
Disciplina::setCodigo(QString c)
{
    this->codigo = c;
}

void
Disciplina::setDescricao(QString d)
{
    this->descricao = d;
}

void
Disciplina::setPeriodo(int p)
{
    this->periodo = p;
}

void Disciplina::setNumAulas(int n)
{
    this->numAulas = n;
}

void Disciplina::setEmenda(QString e)
{
    this->emenda = e;
}

void Disciplina::setBibliografia(QString b)
{
    this->bibliografia = b;
}

void
Disciplina::addTurma(Turma pTurma)
{
    this->listTurma.push_back(&pTurma);
}

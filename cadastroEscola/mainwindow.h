#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "escola.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

bool event(QEvent *e);

private:
    void updateListView(void);
    Ui::MainWindow *ui;
    Escola *escola;

private slots:
    void on_pushButton_clicked();
    void on_actionAluno_triggered();
    void on_actionProfessor_triggered();
    void on_actionCurso_triggered();
    void on_actionTurma_triggered();
    void on_actionDisciplina_triggered();
};
#endif // MAINWINDOW_H

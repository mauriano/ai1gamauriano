#ifndef ESCOLA_H
#define ESCOLA_H

#include "aluno.h"
#include "curso.h"
#include "disciplina.h"
#include "professor.h"
#include "turma.h"

#include <QList>



class Escola
{
public:
    Escola();
    void addAluno(Aluno al);
    void addProfessor(Professor prof);
    void addTurma(Turma tur);
    void addCurso(Curso cur);
    void addDisciplina(Disciplina disc);
    QList<Aluno> getAlunos(void);
    QList<Professor> getProfessores(void);
    QList<Turma> getTurmas(void);
    QList<Curso> getCursos(void);
    QList<Disciplina> getDisciplinas(void);

private:
    QList<Aluno> alunos;
    QList<Professor> professores;
    QList<Turma> turmas;
    QList<Curso> cursos;
    QList<Disciplina> disciplinas;
};

#endif // ESCOLA_H

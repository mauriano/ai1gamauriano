#ifndef DISCIPLINA_H
#define DISCIPLINA_H

#include <qstring.h>
#include <QList>
#include "turma.h"

class Disciplina
{
public:
    Disciplina();
    Disciplina(QString c,
               QString d,
               int p,
               int n,
               QString e,
               QString b);

    QString getCodigo();
    QString getDescricao();
    int getPeriodo();
    int getNumAulas();
    QString getEmenda();
    QString getBibliografia();

    void setCodigo(QString c);
    void setDescricao(QString d);
    void setPeriodo(int p);
    void setNumAulas(int n);
    void setEmenda(QString e);
    void setBibliografia(QString b);

    void addTurma(Turma pTurma);

private:

    QString codigo;
    QString descricao;
    int periodo;
    int numAulas;
    QString emenda;
    QString bibliografia;

    QList <Turma*> listTurma;

};

#endif // DISCIPLINA_H

#include "professorwidget.h"
#include "ui_professorwidget.h"

ProfessorWidget::ProfessorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProfessorWidget)
{
    ui->setupUi(this);
}

ProfessorWidget::ProfessorWidget(QWidget *parent, Escola * escola) :
    QWidget(parent),
    ui(new Ui::ProfessorWidget)
{
    ui->setupUi(this);
    this->escola = escola;
}



ProfessorWidget::~ProfessorWidget()
{
    delete ui;
}

void ProfessorWidget::salvarDados()
{
    float salario = ui->editSalario->text().toFloat();
    QString titulo = ui->editTitulo->text();
    QString contrato = ui->editContrato->text();
    QString matricula = ui->editMatricula->text();

    QString nome = ui->editNome->text();
    QDate data = QDate::fromString(ui->editNascimento->text(),"dd/MM/yyyy");
    QString email = ui->editEmail->text();
    QString end = ui->editEndereco->text();
    QString tel = ui->editTelefone->text();
    QString foto = ui->editFotografia->text();
    QString cpf = ui->editCpf->text();
    QString rg = ui->editRg->text();
    Pessoa *pessoa = new Pessoa(nome,data,email,end,tel,foto,cpf,rg);

    Professor *professor = new Professor(matricula, titulo, contrato, salario, *pessoa);

    escola->addProfessor(*professor);

}

void ProfessorWidget::on_pushButton_clicked()
{
    salvarDados();
    this->hide();
}

#include "professor.h"

Professor::Professor()
{
    this->setSalario(0);
    this->setTitulo("");
    this->setTipoContrato("");
    this->setMatricula("");
}

Professor::Professor(QString mat,
          QString t,
          QString ti,
          float s,
          Pessoa pessoa)
{
    this->setSalario(s);
    this->setTitulo(ti);
    this->setTipoContrato(t);
    this->setMatricula(mat);
    this->setPessoa(pessoa);
}

void Professor::setPessoa(Pessoa pessoa){
    this->setNome(pessoa.getNome());
    this->setDataNascimento(pessoa.getDataNascimento());
    this->setEmail(pessoa.getEmail());
    this->setEndereco(pessoa.getEndereco());
    this->setTelefone(pessoa.getTelefone());
    this->setFotografia(pessoa.getFotografia());
    this->setRg(pessoa.getRg());
}


QString Professor::getMatricula()
{
    return this->matricula;
}

QString
Professor::getTitulo()
{
    return this->titulo;
}

QString
Professor::getTipoContrato()
{
    return this->tipoContrato;
}

float
Professor::getSalario()
{
    return this->salario;
}

void
Professor::setMatricula(QString mat)
{
    this->matricula = mat;
}

void
Professor::setTitulo(QString t)
{
    this->titulo = t;
}

void
Professor::setTipoContrato(QString ti)
{
    this->tipoContrato = ti;
}

void
Professor::setSalario(float s)
{
    this->salario = s;
}

#include "mainwindow.h"
#include "professorwidget.h"
#include "ui_mainwindow.h"
#include "alunowidget.h"
#include "disciplinawidget.h"
#include "turmawidget.h"
#include "cursowidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //escola = new Escola();
    this->escola = new Escola;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateListView(void)
{
    int i;
    ui->listProfessor->setText("");
    for (i=0;i<this->escola->getProfessores().size();i++) {
        ui->listProfessor->append(escola->getProfessores().takeAt(i).getNome());
    }
    ui->listAluno->setText("");
    for (i=0;i<this->escola->getAlunos().size();i++) {
        ui->listAluno->append(escola->getAlunos().takeAt(i).getNome());
    }
    ui->listCurso->setText("");
    for (i=0;i<this->escola->getCursos().size();i++) {
        ui->listCurso->append(escola->getCursos().takeAt(i).getDescricao());
    }
    ui->listTurma->setText("");
    for (i=0;i<this->escola->getTurmas().size();i++) {
        ui->listTurma->append(escola->getTurmas().takeAt(i).getDescricao());
    }
    ui->listDisciplina->setText("");
    for (i=0;i<this->escola->getDisciplinas().size();i++) {
        ui->listDisciplina->append(escola->getDisciplinas().takeAt(i).getDescricao());
    }
}

void MainWindow::on_pushButton_clicked()
{
    this->updateListView();
}

void MainWindow::on_actionAluno_triggered()
{
    AlunoWidget *wi = new AlunoWidget(nullptr, this->escola);
    wi->show();

}

void MainWindow::on_actionProfessor_triggered()
{
    ProfessorWidget *wi = new ProfessorWidget(nullptr, this->escola);
    wi->show();
}

void MainWindow::on_actionCurso_triggered()
{
    CursoWidget *wi = new CursoWidget(nullptr, this->escola);
    wi->show();
}

void MainWindow::on_actionTurma_triggered()
{
    TurmaWidget *wi = new TurmaWidget(nullptr, this->escola);
    wi->show();
}

void MainWindow::on_actionDisciplina_triggered()
{
    DisciplinaWidget *wi = new DisciplinaWidget(nullptr, this->escola);
    wi->show();
}

bool MainWindow::event(QEvent *e)
{
    switch(e->type())
    {
    case QEvent::WindowActivate:
        // gained focus
        this->updateListView();
        break ;
    default:
        break;
    } ;
    return QMainWindow::event(e);
}



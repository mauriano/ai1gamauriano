#ifndef PROFESSORWIDGET_H
#define PROFESSORWIDGET_H

#include "escola.h"

#include <QWidget>
#include <QList>

namespace Ui {
class ProfessorWidget;
}

class ProfessorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ProfessorWidget(QWidget *parent = nullptr);
    explicit ProfessorWidget(QWidget *parent = nullptr, Escola *escola = nullptr);
    ~ProfessorWidget();


private slots:
    void on_pushButton_clicked();

private:
    Ui::ProfessorWidget *ui;
    void salvarDados();
    Escola* escola;
};

#endif // PROFESSORWIDGET_H

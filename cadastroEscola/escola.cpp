#include "escola.h"

Escola::Escola()
{

}

void
Escola::addAluno(Aluno al)
{
    this->alunos.push_back(al);
}

void
Escola::addProfessor(Professor prof)
{
    this->professores.push_back(prof);
}

void
Escola::addTurma(Turma tur)
{
    this->turmas.push_back(tur);
}

void
Escola::addCurso(Curso cur)
{
    this->cursos.push_back(cur);
}

void
Escola::addDisciplina(Disciplina disc)
{
    this->disciplinas.push_back(disc);
}

QList<Aluno>
Escola::getAlunos(void)
{
    return this->alunos;
}

QList<Professor>
Escola::getProfessores(void)
{
    return this->professores;
}

QList<Turma>
Escola::getTurmas(void)
{
    return this->turmas;
}

QList<Curso>
Escola::getCursos(void)
{
    return this->cursos;
}

QList<Disciplina>
Escola::getDisciplinas(void)
{
    return this->disciplinas;
}

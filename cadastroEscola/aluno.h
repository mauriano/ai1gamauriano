#ifndef ALUNO_H
#define ALUNO_H

#include<pessoa.h>

class Aluno: public Pessoa
{
public:
    Aluno();

    Aluno(QString mat,
          int a,
          int s,
          QString sit,
          Pessoa pessoa);

    QString getMatricula();
    int getAnoInicio();
    int getSemestreInicio();
    QString getSituacao();

    void setMatricula(QString mat);
    void setAnoInicio(int a);
    void setSemestreInicio(int s);
    void setSituacao(QString sit);

    void setPessoa(Pessoa pessoa);

private:
    QString matricula;
    int anoInicio;
    int semestreInicio;
    QString situacao;

};

#endif // ALUNO_H

#include "turma.h"

Turma::Turma()
{

}

void
Turma::addAluno(Aluno* al)
{
    this->listAluno.push_back(al);
}

void
Turma::setProfessor(Professor* pr)
{
    this->prof = pr;
}

QString Turma::getDescricao(){
    return this->descricao;
}

void Turma::setAno(int a){
    this->ano = a;
}
void Turma::setSemestre(int s){
    this->semestre = s;
}
void Turma::setDescricao(QString d){
    this->descricao = d;
}
void Turma::setNumMaxAlunos(int n){
    this->numMaxAlunos = n;
}

Turma::Turma(int a,int s, QString d, int n){
    this->setAno(a);
    this->setSemestre(s);
    this->setDescricao(d);
    this->setNumMaxAlunos(n);
}

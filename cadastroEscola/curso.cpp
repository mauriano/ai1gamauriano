#include "curso.h"

Curso::Curso()
{
    this->setCodigo(0);
    this->setDescricao("");
    this->setCargaHoraria(0);
    this->setQtdPeriodos(0);
    this->setTpoCurso("");
}

Curso::Curso(int c,
      QString d,
      int ch,
      int q,
      QString t)
{
    this->setCodigo(c);
    this->setDescricao(d);
    this->setCargaHoraria(ch);
    this->setQtdPeriodos(q);
    this->setTpoCurso(t);
}

int
Curso::getCodigo()
{
    return this->codigo;
}

QString
Curso::getDescricao()
{
    return this->descricao;
}

int
Curso::getCargaHoraria()
{
    return this->cargaHoraria;
}

int
Curso::getQtdPeriodos()
{
    return this->qtdPeriodos;
}

QString
Curso::getTpoCurso()
{
    return this->tpoCurso;
}

void
Curso::setCodigo(int c)
{
    this->codigo = c;
}

void
Curso::setDescricao(QString d)
{
   this->descricao = d;
}

void
Curso::setCargaHoraria(int ch)
{
    this->cargaHoraria = ch;
}

void
Curso::setQtdPeriodos(int q)
{
    this->qtdPeriodos = q;
}

void
Curso::setTpoCurso(QString t)
{
    this->tpoCurso = t;
}

void
Curso::addDisciplina(Disciplina pDisc)
{
    this->listDisciplinas.push_back(&pDisc);
}

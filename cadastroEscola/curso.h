#ifndef CURSO_H
#define CURSO_H

#include <qstring.h>
#include <QList>
#include "disciplina.h"


class Curso
{
public:
    Curso();

    Curso(int c,
          QString d,
          int ch,
          int q,
          QString t);

    int getCodigo();
    QString getDescricao();
    int getCargaHoraria();
    int getQtdPeriodos();
    QString getTpoCurso();

    void setCodigo(int c);
    void setDescricao(QString d);
    void setCargaHoraria(int ch);
    void setQtdPeriodos(int q);
    void setTpoCurso(QString t);

    void addDisciplina(Disciplina pDisc);

private:
    int codigo;
    QString descricao;
    int cargaHoraria;
    int qtdPeriodos;
    QString tpoCurso;
    QList<Disciplina*> listDisciplinas;
};

#endif // CURSO_H

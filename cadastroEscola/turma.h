#ifndef TURMA_H
#define TURMA_H

#include <qstring.h>
#include <QList>
#include "aluno.h"
#include "professor.h"



class Turma
{
public:
    Turma();
    Turma(int a,
          int s,
          QString d,
          int n);

    void setAno(int a);
    void setSemestre(int s);
    void setDescricao(QString d);
    void setNumMaxAlunos(int n);

    int getAno();
    int getSemestre();
    QString getDescricao();
    int getNumMaxAlunos();

    void addAluno(Aluno* al);
    void setProfessor(Professor* pr);

private:
    int ano;
    int semestre;
    QString descricao;
    int numMaxAlunos;

    QList <Aluno*> listAluno;
    Professor* prof;
};

#endif // TURMA_H
